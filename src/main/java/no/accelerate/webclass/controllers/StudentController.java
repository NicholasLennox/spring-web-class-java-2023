package no.accelerate.webclass.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.webclass.mappers.StudentMapper;
import no.accelerate.webclass.models.Student;
import no.accelerate.webclass.models.dtos.students.StudentGetDTO;
import no.accelerate.webclass.models.dtos.students.StudentListDTO;
import no.accelerate.webclass.services.student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("api/v1/students")
public class StudentController {

    private StudentService studentService;
    private final StudentMapper studentMapper;

    public StudentController(StudentService studentService, StudentMapper studentMapper) {
        this.studentService = studentService;
        this.studentMapper = studentMapper;
    }

    @GetMapping
    @Operation(summary = "Gets all the students")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentGetDTO.class)))
                    }
            )
    })
    public ResponseEntity findAll() {
        return ResponseEntity.ok(
                        studentMapper.studentToStudentGetDto(
                                studentService.findAll())
                );
    }

    @Operation(summary = "Get a student by their ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = StudentGetDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Student does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class)) })
    })
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id) {
        return ResponseEntity.ok(studentService.findById(id));
    }

    @PostMapping
    @Operation(summary = "Adds a new student")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed Request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class)) })
    })
    public ResponseEntity add(@RequestBody Student student) {
        studentService.add(student);
        URI uri = URI.create("students/" + student.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a student by their ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Student does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class)) }),
            @ApiResponse(responseCode = "400",
                description = "Malformed Request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class)) })
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody Student student, @PathVariable int id) {
        if(id != student.getId())
            return ResponseEntity.badRequest().build();
        studentService.update(student);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{id}/professor")
    public ResponseEntity updateProf() {
        return null;
    }

    @DeleteMapping
    public ResponseEntity delete() {
        return null;
    }
}
