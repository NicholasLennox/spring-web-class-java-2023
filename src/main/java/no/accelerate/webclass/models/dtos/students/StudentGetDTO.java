package no.accelerate.webclass.models.dtos.students;

import java.util.Set;

public class StudentGetDTO {
    private int id;
    private String name;
    private int project;
    private int professor;
    private Set<Integer> subjects;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProject() {
        return project;
    }

    public void setProject(int project) {
        this.project = project;
    }

    public int getProfessor() {
        return professor;
    }

    public void setProfessor(int professor) {
        this.professor = professor;
    }

    public Set<Integer> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Integer> subjects) {
        this.subjects = subjects;
    }
}
