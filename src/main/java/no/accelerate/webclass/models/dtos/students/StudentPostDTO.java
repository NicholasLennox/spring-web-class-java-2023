package no.accelerate.webclass.models.dtos.students;

import jakarta.persistence.Column;

public class StudentPostDTO {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
