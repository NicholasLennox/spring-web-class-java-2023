package no.accelerate.webclass.mappers;

import no.accelerate.webclass.models.Student;
import no.accelerate.webclass.models.Subject;
import no.accelerate.webclass.models.dtos.students.StudentGetDTO;
import no.accelerate.webclass.models.dtos.students.StudentListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class StudentMapper {
    public abstract StudentListDTO studentToStudentListDto(Student student);
    public abstract Collection<StudentListDTO> studentToStudentListDto(Collection<Student> student);

    @Mapping(target = "project", source = "project.id")
    @Mapping(target = "professor", source = "professor.id")
    @Mapping(target = "subjects", source = "subjects", qualifiedByName = "subjectsToIds")
    public abstract StudentGetDTO studentToStudentGetDto(Student student);

    public abstract Collection<StudentGetDTO> studentToStudentGetDto(Collection<Student> student);

    @Named("subjectsToIds")
    public Set<Integer> mapSubjectsToIds(Set<Subject> subjectSet) {
        if(subjectSet != null)
            return subjectSet.stream()
                    .map(s -> s.getId())
                    .collect(Collectors.toSet());
        return null;
    }
}
