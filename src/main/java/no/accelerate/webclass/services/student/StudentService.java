package no.accelerate.webclass.services.student;

import no.accelerate.webclass.models.Student;
import no.accelerate.webclass.services.CRUDService;

public interface StudentService extends CRUDService<Student, Integer> {
    // Extra business logic
    void updateProfessor(int studentId, int professorId);
}
