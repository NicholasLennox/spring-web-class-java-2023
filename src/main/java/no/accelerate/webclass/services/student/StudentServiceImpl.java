package no.accelerate.webclass.services.student;

import jakarta.transaction.Transactional;
import no.accelerate.webclass.exceptions.StudentNotFoundException;
import no.accelerate.webclass.models.Student;
import no.accelerate.webclass.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student findById(Integer id) {
        return studentRepository
                .findById(id)
                .orElseThrow(() -> new StudentNotFoundException(id));
    }

    @Override
    public Collection<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student add(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    public Student update(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if(studentRepository.existsById(id)) {
            Student stud = studentRepository.findById(id).get();
            stud.setProfessor(null);
            stud.getProject().setStudent(null);
            studentRepository.delete(stud);
        }
    }

    @Override
    @Transactional
    public void delete(Student entity) {
        if(studentRepository.existsById(entity.getId())) {
            Student stud = studentRepository.findById(entity.getId()).get();
            stud.setProfessor(null);
            stud.getProject().setStudent(null);
            studentRepository.delete(stud);
        }
    }

    @Override
    @Transactional
    public void updateProfessor(int studentId, int professorId) {
        studentRepository.updateStudentsProfessorById(studentId,professorId);
    }
}
